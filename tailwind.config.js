/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

const fontFamilies = {
	AkzidenzGrotes: 'Akzidenz-Grotesk, Helvetica, Arial, sans-serif'
}

const colors = {
	red: '#E94235',
	blue: '#293894',
	black: '#1B1B1B',
	white: '#FFFFFF',
	lightgrey: '#DFE0D9',
	darkgrey: '#6B6B65',
	lightergrey: '#CFD1C7'
}

const fontSize = {
	xxs: '0.625rem',
	xs: '0.75rem',
	sm: '0.875rem',
	base: '1rem',
	lg: '1.125rem',
	xl: '1.25rem',
	'2xl': '1.5rem',
	'3xl': '1.875rem',
	'4xl': '2.25rem',
	'5xl': '3rem',
	'6xl': '4rem'
}

module.exports = {
	theme: {
		colors: {
			...colors
		},
		fontFamily: {
			...fontFamilies
		},
		letterSpacing: {
			tightest: '-2px',
			tight: '-0.5px',
			spaced: '1.5px'
		},
		fontSize: {
			...fontSize
		},
		extend: {
			lineHeight: {
				'18': '1.173rem',
				'44': '2.75rem'
			}
		}
	},
	variants: {},
	plugins: []
}
